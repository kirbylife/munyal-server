import base64
import json
import os
import shutil

from websocket_server import WebsocketServer

from misc import path_join

FOLDER = "/home/kirbylife/Munyal_server"
# FOLDER = "/home/kirbylife/Proyectos/Munyal_server/folder"


def connect(client, server):
    print(client["handler"].send_message("Mensaje desde el servidor"))
    print("Usuario conectado")


def message(client, server, message):
    data = json.loads(message)
    print("modifying file:", data["name"])
    full_path = path_join(FOLDER, data["name"])
    if data["is_file"]:
        if data["action"] == "add":
            directory = os.path.split(full_path)[0]
            if not os.path.exists(directory):
                os.makedirs(directory, exist_ok=True)
            with open(full_path, "wb") as f:
                f.write(base64.b85decode(data["file"].encode("ascii")))
        else:
            os.remove(full_path)
    else:
        if data["action"] == "add":
            os.mkdir(full_path)
        else:
            shutil.rmtree(full_path)
    server.send_message_to_all(json.dumps(data))
    print(data)


if __name__ == "__main__":
    server = WebsocketServer(12345, "0.0.0.0")
    server.set_fn_new_client(connect)
    server.set_fn_message_received(message)
    server.run_forever()

# from flask import Flask
# from flask_socketio import SocketIO, emit
#
# app = Flask(__name__)
# app.config["SECRET_KEY"] = "123123123"
# socketio = SocketIO(app)
#
#
# @socketio.on("connect", namespace="/ws")
# def ws_connect(message):
# print("received json: " + str(message))
#
#
# @socketio.on("add", namespace="/ws")
# def ws_add(message):
# emit("add", {"url": "/test"})
#
#
# @socketio.on("delete", namespace="/ws")
# def ws_delete(message):
# emit("delete", {"url": "/test"})
#
#
# if __name__ == "__main__":
# socketio.run(app)
