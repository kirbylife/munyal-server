import os

def path_join(*items):
    return os.path.join(*items).replace("\\", "/")
