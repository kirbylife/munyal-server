x = '''                                   _
 _ __ ___  _   _ _ __  _   _  __ _| |
| '_ ` _ \| | | | '_ \| | | |/ _` | |
| | | | | | |_| | | | | |_| | (_| | |
|_| |_| |_|\__,_|_| |_|\__, |\__,_|_|
                       |___/

       ¡Bienvenido a Munyal!

>> Parece que es la primera vez que corres el servidor, te importa si te hacemos unas preguntas?

[*] Detectando carpeta de acción: ~/.munyal_server/
[*] Conección a internet

>> Ya cuentas con un nombre de servidor?
[S/N]>> n

>> Genial, un nuevo usuario :D
>> Que nombre te gustaría para tu servidor? El nombre que elijas será unicamente tuyo.
[piltla]>> servidor_1

>> Excelente, ese nombre parece estar disponible.
Este es tu uuid: 98bce7f8-652d-4541-a6f6-5857b8c888d8
PRECAUCIÓN
#######################################################
# ESCRIBE TU uuid EN UN LUGAR QUE NO PUEDAS PERDERLO  #
# YA QUE SI QUIERES VOLVER A REUTILIZAR TU NOMBRE SE  #
# TE PEDIRÁ, ASÍ QUE CUIDALO MUCHO.                   #
#######################################################

Ahora selecciona la contraseña de tu servidor, procura que sea segura.
[]>> *****************

Listo, parece todo está perfecto listo, disfruta de tu servidor.

2019-04-22 14:52:32 [DEBUG]: Start server
'''
print(x)
